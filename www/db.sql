use minitrello;

CREATE TABLE IF NOT EXISTS users(
  id INT NOT NULL AUTO_INCREMENT,
  email VARCHAR(255) NOT NULL,
  password VARCHAR(255) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB;
CREATE UNIQUE INDEX user_email ON users(email);

CREATE TABLE IF NOT EXISTS projects(
  id INT NOT NULL AUTO_INCREMENT,
  ownerId INT NOT NULL,
  title VARCHAR(255) NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT ownerId FOREIGN KEY ownerId (ownerId) REFERENCES users(id)
) ENGINE=InnoDB;

ALTER TABLE projects ADD COLUMN archived bool after id;
UPDATE projects set archived = 0;
