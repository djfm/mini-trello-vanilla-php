<?php

$dbh = require('./dbh.php');

$sql = file_get_contents('./db.sql');

try {
  $ok = $dbh->exec($sql);
  if ($ok) {
    echo "Success!";
  } else {
    echo "Failed.";
  }
} catch (PDOException $e) {
  print "Error !: " . $e->getMessage() . "<br/>";
  die();
}
