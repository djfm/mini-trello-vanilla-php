<?php
use DJFM\Framework\Request;
use DJFM\Framework\Response;
use DJFM\Framework\Router;
use DJFM\Framework\DB;

require_once implode(
  DIRECTORY_SEPARATOR,
  [__DIR__, 'vendor', 'autoload.php']
);

session_start();

$dbh = require('./dbh.php');
$db = new DB($dbh);

$hash = function ($password) {
  return md5('IknowThisIsTotallyUnsafeEvenWIthGoodSalt'.$password);
};

$container = [
  'db' => $db,
  'hash' => $hash,
];

$loggedIn = isset($_SESSION['user']);
$request = Request::fromGlobals()->setParam('loggedIn', $loggedIn);
$response = new Response();
$response
  ->setTemplatesDir(__DIR__ . DIRECTORY_SEPARATOR . 'templates')
  ->setLayout('layout.php')
  ->setTemplateArg('userEmail', isset($_SESSION['user']) ? $_SESSION['user']['email'] : null)
  ->setTemplateArg('loggedIn', isset($_SESSION['user']))
;

$router = new Router();

$router->on('GET', '/', function ($request, $response) {
  return $response->render('index.php');
});

$router->on('GET', '/register', function ($request, $response) {
  return $response->render('register.php');
});
$router->on('POST', '/register', require('./src/Controller/registerPost.php'));

$router->on('GET', '/login', function ($request, $response) {
  return $response->render('login.php');
});
$router->on('POST', '/login', require('./src/Controller/loginPost.php'));

$router->on('GET', '/logout', function ($request, $response) {
  unset($_SESSION['user']);
  return $response->redirect('/');
});

$router->on(
  'GET', '/projects', require('./src/Controller/projectsGet.php')
);
$router->on(
  'GET', '/projects/new', require('./src/Controller/projectsNew.php')
);

$router->on(
  'GET', '/projects/:id', require('./src/Controller/projectEdit.php')
);
$router->on(
  'GET', '/projects/:id/archive', require('./src/Controller/projectArchive.php')
);
$router->on(
  'POST', '/projects/:id', require('./src/Controller/projectPost.php')
);

$router->on(
  'POST', '/projects', require('./src/Controller/projectsPost.php')
);

$result = $router->run($request, $response, $container);

echo $result;
