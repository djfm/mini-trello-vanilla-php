<?php

use PHPUnit\Framework\TestCase;

use DJFM\Framework\Request;
use DJFM\Framework\Response;
use DJFM\Framework\Router;

final class RouterTest extends TestCase
{
  public function testCorrectRouteIsRendered()
  {
    $router = new Router();

    $router->on('GET', '/login/bob', function ($request) {
      return 'nope';
    });

    $router->on('GET', '/login', function ($request) {
      return 'ho';
    });

    $router->on('GET', '/test', function ($request) {
      return 'test';
    });

    $router->on('POST', '/login', function ($request) {
      return 'hi';
    });

    $router->on('GET', '/', function ($request) {
      return 'hu';
    });

    $request = new Request();
    $response = new Response();

    $request->setMethod('POST');
    $request->setPath('/login');
    $this->assertEquals(
      'hi',
      $router->run($request, $response)
    );

    $request->setMethod('GET');
    $request->setPath('/login/bob');
    $this->assertEquals(
      'nope',
      $router->run($request, $response)
    );
  }

  public function testRouteWithParams()
  {
    $router = new Router();

    $router->on('GET', '/login/:x', function ($request) {
      return 'yes';
    });

    $request = new Request();
    $response = new Response();

    $request->setMethod('GET');
    $request->setPath('/login/bob');
    $this->assertEquals(
      'yes',
      $router->run($request, $response)
    );
  }
}
