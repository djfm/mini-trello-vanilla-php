<?php

use PHPUnit\Framework\TestCase;

use DJFM\Framework\Request;

final class RequestTest extends TestCase
{
  public function testMethodIsPopulated()
  {
    $server = [
      'REQUEST_METHOD' => 'POST'
    ];

    $r = (new Request())->fromArray($server);
    $this->assertEquals('POST', $r->getMethod());
  }

  public function testPathIsPopulated()
  {
    $r = new Request([
      'REQUEST_URI' => '/bob'
    ]);
    $this->assertEquals('/bob', $r->getPath());
  }
}
