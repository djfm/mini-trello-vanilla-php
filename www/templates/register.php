<section>
  <h1>Register</h1>
  <form method="POST">
    <? if ($error): ?>
      <div class="error"><?= $error ?></div>
    <? endif; ?>
    <label for="email">
      Email:
      <input
        id="email"
        name="email"
        type="email"
        value="<?= $email ?>"
      >
    </label>
    <label for="password">
      Password:
      <input
        id="password"
        name="password"
        type="password"
        value="<?= $password ?>"
      >
    </label>
    <label for="confirm">
      Confirm:
      <input
        id="confirm"
        name="confirm"
        type="password"
        value="<?= $confirm ?>"
      >
    </label>
    <button type="submit">Register</button>
  </form>
</section>
