<section>
  <h1>Project Form</h1>
  <form method="POST" action="/projects">
    <? if ($error): ?>
      <div class="error"><?= $error ?></div>
    <? endif; ?>
    <label for="title">
      Title:
      <input
        id="title"
        name="title"
        type="text"
        value="<?= $title ?>"
    </label>
    <button type="submit">Save</button>
  </form>
</section>
