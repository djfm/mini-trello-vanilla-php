<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Mini-Trello</title>
    <link rel="stylesheet" href="/assets/styles.css">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  </head>
  <body>
    <div class="header">
      <h1>Mini-Trello</h1>
        <nav>
          <a href="/projects">Projectcs</a>
        </nav>
        <div>
        <? if ($userEmail): ?>
          <?= $userEmail ?> <a href="/logout">[logout]</a>
        <? else: ?>
          <a href="/login">Log in</a>
          <a href="/register">Register</a>
        <? endif; ?>
      </div>
    </div>
    <?= $body ?>
  </body>
</html>
