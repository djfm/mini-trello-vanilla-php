<section>
  <h1>Project <?= $title ?></h1>
  <form method="POST" action="/projects/<?= $id ?>">
    <? if ($error): ?>
      <div class="error"><?= $error ?></div>
    <? endif; ?>
    <label for="title">
      Title:
      <input
        id="title"
        name="title"
        type="text"
        value="<?= $title ?>"
    </label>
    <input type="hidden" value="<?= $id ?>">
    <button type="submit">Save</button>
  </form>
</section>
