<section>
  <h1>Log in</h1>
  <form method="POST">
    <? if ($error): ?>
      <div class="error"><?= $error ?></div>
    <? endif; ?>
    <label for="email">
      Email:
      <input
        id="email"
        name="email"
        type="email"
        value="<?= $email ?>"
      >
    </label>
    <label for="password">
      Password:
      <input
        id="password"
        name="password"
        type="password"
        value="<?= $password ?>"
      >
    </label>
    <button type="submit">Log in</button>
  </form>
</section>
