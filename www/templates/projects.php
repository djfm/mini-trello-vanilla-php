<section>
<h1>Projects</h1>
<? if ($loggedIn): ?>
  <div>
    <? foreach ($projects as $project): ?>
      <div class="project row">
        <a
          href="/projects/<?=$project['id']?>"><?= $project['title'] ?> </a>
        <a
        href="/projects/<?=$project['id']?>/archive"
        class="button">archive</a>
      </div>
    <? endforeach; ?>
  </div>

  <a class="button" href="/projects/new">Create Project</a>
<? else: ?>
  <div>Please <a href="/login">log in</a> to access projects.</div>
<? endif; ?>
</section>
