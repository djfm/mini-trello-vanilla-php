<?php

return function ($request, $response, $container) {
  $userId = $_SESSION['user']['id'];
  $title = $request->getPostData()['title'];

  if (!$title) {
    return $response->render('projectNew.php', [
      'error' => 'Title is required.'
    ]);
  }

  $container['db']->execute(
    'INSERT INTO projects (title, ownerId, archived) VALUES (:title, :ownerId, 0)',
    [
      'title' => $title,
      'ownerId' => $userId,
    ]
  );

  return $response->redirect('/projects');
};
