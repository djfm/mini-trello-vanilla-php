<?php

return function ($request, $response, $container) {
  $project = $container['db']->selectOne(
    'SELECT * FROM projects WHERE id = :id',
    ['id' => $request->getParam('id')]
  );
  return $response->render('projectEdit.php', $project);
};
