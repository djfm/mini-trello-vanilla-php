<?php

return function ($request, $response, $container) {
  $data = $request->getPostData();
  if ($data['password'] !== $data['confirm']) {
    return $response->render('register.php', array_merge($data, [
      'error' => 'Password does not match confirmation.',
    ]));
  }

  $user = $container['db']->selectOne(
    'SELECT * FROM users WHERE email=:email',
    ['email' => $data['email']]
  );
  if ($user) {
    return $response->render('register.php', array_merge($data, [
      'error' => 'A user with this email address already exists.',
    ]));
  }

  $container['db']->execute(
    'INSERT INTO users (email, password) VALUES (:email, :password)',
    [
      'email' => $data['email'],
      'password' => $container['hash']($data['password'])
    ]
  );

  $user = $container['db']->selectOne(
    'SELECT * FROM users WHERE email=:email',
    ['email' => $data['email']]
  );
  $_SESSION['user'] = $user;

  return $response->redirect('/');
};
