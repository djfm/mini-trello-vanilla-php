<?php

return function ($request, $response, $container) {
  if (isset($_SESSION['user'])) {
    $ownerId = $_SESSION['user']['id'];

    $pstm = $container['db']->execute(
      'SELECT * FROM projects WHERE ownerId=:ownerId AND archived=0',
      ['ownerId' => $ownerId]
    );

    $projects = [];
    while ($p = $pstm->fetch()) {
      $projects[] = $p;
    }


    return $response->render('projects.php', [
      'projects' => $projects
    ]);
  }

  return $response->render('projects.php');
};
