<?php

return function ($request, $response, $container) {
  $container['db']->execute(
    'UPDATE projects SET archived = 1 where id = :projectId',
    ['projectId' => $request->getParam('id')]
  );
  return $response->redirect("/projects");
};
