<?php

return function ($request, $response, $container) {
  $ownerId = $_SESSION['user']['id'];
  $title = $request->getPost('title');
  $projectId = $request->getParam('id');

  if (!$title) {
    return $response->render('projectsPatch.php', [
      'error' => 'Title is required.'
    ]);
  }

  $r = $container['db']->execute(
    'UPDATE projects SET title=:title WHERE id=:projectId AND ownerId=:ownerId',
    [
      'title' => $title,
      'ownerId' => $ownerId,
      'projectId' => $projectId
    ]
  );

  return $response->redirect("/projects/${projectid}");
};
