<?php

return function ($request, $response, $container) {
  $data = $request->getPostData();
  $password = $container['hash']($data['password']);
  $user = $container['db']->selectOne(
    'SELECT * FROM users WHERE email = :email and password = :password',
    [
      'email' => $data['email'],
      'password' => $password,
    ]
  );
  if ($user) {
    $_SESSION['user'] = $user;
    return $response->redirect('/');
  } else {
    return $response->render('login.php', array_merge($data, [
      'error' => 'Invalid email or password'
    ]));
  }
};
