<?php

namespace DJFM\Framework;

class Response
{
  private $templatesDir;
  private $layout;
  private $templateArgs = [];

  public function setTemplatesDir($dir)
  {
    $this->templatesDir = $dir;
    return $this;
  }

  public function setLayout($layout)
  {
    $this->layout = $layout;
    return $this;
  }

  private function renderTemplate($template, $args = [])
  {
    extract($args);
    ob_start();
    require($this->templatesDir . DIRECTORY_SEPARATOR . $template);
    return ob_get_clean();
  }

  public function render($template, $args = [])
  {
    $arguments = array_merge($this->templateArgs, $args);

    $body = $this->renderTemplate($template, $arguments);
    if ($this->layout) {
      return $this->renderTemplate($this->layout, array_merge(
        $arguments,
        [
          'body' => $body
        ]
      ));
    }
    return $body;
  }

  public function redirect($to)
  {
    header('Location: ' . $to);
    die();
  }

  public function setTemplateArg($key, $value)
  {
    $this->templateArgs[$key] = $value;
    return $this;
  }

  public function getTemplateArgs()
  {
    return $this->templateArgs;
  }
}
