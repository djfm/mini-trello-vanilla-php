<?php
namespace DJFM\Framework;

use DJFM\Framework\Request;
use DJFM\Framework\Response;

class Router
{
  private $routes = [];
  private $params = [];

  private function pathToArray($path)
  {
    $bits = array_values(array_filter(
      explode('/', $path),
      function ($bit) {
        return $bit !== '';
      }
    ));
    return $bits;
  }

  public function on($method, $path, callable $controller)
  {
    $this->routes[] = [
      'method' => $method,
      'path' => $this->pathToArray($path),
      'controller' => $controller
    ];
  }

  private function matchPathPart($routePart, $requestPart)
  {
    if ($routePart[0] === ':') {
      $this->params[substr($routePart, 1)] = $requestPart;
      return true;
    }
    return $routePart === $requestPart;
  }

  private function cleanParam($p) {
    if ($p[0] === ':') {
      return substr($p, 1);
    }
    return $p;
  }

  private function match($route, $request) {
    if ($route['method'] !== $request->getMethod()) {
      return false;
    }

    $rPath = $this->pathToArray($request->getPath());

    if (count($rPath) !== count($route['path'])) {
      return false;
    }

    $this->params = [];
    for ($i = 0; $i < count($rPath); $i += 1) {
      if (!$this->matchPathPart($route['path'][$i], $rPath[$i])) {
        return false;
      }
    }
    $request->setParams($this->params);

    return true;
  }

  public function run(Request $request, Response $response, array $container = [])
  {
    foreach ($this->routes as $route) {
      if ($this->match($route, $request)) {
        return call_user_func($route['controller'], $request, $response, $container);
      }
    }
  }
}
