<?php

namespace DJFM\Framework;
use PDO;

class DB
{
  private $dbh;

  public function __construct(PDO $dbh)
  {
    $this->dbh = $dbh;
  }

  public function execute($sql, array $args)
  {
    $stm = $this->dbh->prepare($sql);
    if (!$stm->execute($args)) {
      return false;
    }
    return $stm;
  }

  public function selectOne($sql, array $args)
  {
    $stm = $this->execute($sql, $args);
    if (!$stm) {
      return false;
    }
    return $stm->fetch();
  }
}
