<?php

namespace DJFM\Framework;

class Request
{
  private $method;
  private $path;
  private $postData;
  private $params = [];

  public static function fromGlobals()
  {
    return (new Request($_SERVER))
      ->setPostData($_POST)
    ;
  }

  public function __construct(array $server = [])
  {
    return $this->fromArray($server);
  }

  public function setMethod($method)
  {
    $this->method = $method;
    return $this;
  }

  public function getMethod()
  {
    return $this->method;
  }

  public function setPath($path)
  {
    $this->path = $path;
    return $this;
  }

  public function getPath()
  {
    return $this->path;
  }

  public function setPostData($postData)
  {
    $this->postData = $postData;
    return $this;
  }

  public function getPostData()
  {
    return $this->postData;
  }

  public function getPost($key)
  {
    return $this->postData[$key];
  }

  public function fromArray(array $server)
  {
    $this->method = @$server['REQUEST_METHOD'];
    $this->path = @$server['REQUEST_URI'];
    return $this;
  }

  public function setParams(array $params)
  {
    $this->params = $params;
    return $this;
  }

  public function setParam($key, $value)
  {
    $this->params[$key] = $value;
    return $this;
  }

  public function getParam($p)
  {
    return $this->params[$p];
  }
}
